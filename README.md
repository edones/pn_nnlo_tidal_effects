# PN_NNLO_tidal_effects

$$\begin{align}
L_N &=-m_1c^2 + \frac{1}{2}m_1 v_1^2 + \frac{1}{2} \frac{\alpha \tilde{G}m_1 m_2}{r_{12}} + [1\leftrightarrow 2] \nonumber \\ 
L_{1PN} &= \frac{\alpha^2 \tilde{G}^2 m_1^2 m_2}{r_{12}^2} \left(-\tfrac{1}{2}-\bar{\beta}_2 \right) + \frac{1}{8}m_1v_1^4 \nonumber \\
        & \quad + \alpha \tilde{G} m_1 m_2 \Bigl[(2+\bar{\gamma})(a_1.n_{12}) \nonumber \\
        & \quad +\frac{1}{r_{12}}\Bigl ( (-2-\bar{\gamma})(n_{12}.v_1)^2 +\frac{1}{4} (-15-8 \bar{\gamma})
        (v_1.v_2) 
        +\frac{1}{4} (7+4 \bar{\gamma}) (n_{12}.v_1)(n_{12}.v_2)  
        +\frac{1}{2} (7+4 \bar{\gamma})v_1^2 \Bigr )\Bigr] +[1\leftrightarrow 2] 
\end{align}$$


$$\alpha \tilde{G} m_1 m_2 \Bigl[(2+\bar{\gamma})(a_1.n_{12})$$

